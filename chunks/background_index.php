<style>
    #index_background {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background-image: url('/i/10.jpeg');
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        opacity: 0.4;
        filter: blur(4px);
        z-index: -1;
    }

</style>

<div id="index_background"></div>
