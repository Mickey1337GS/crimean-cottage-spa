<style>
    form {
        z-index: 1;
    }

    form input {
        display: block;
        padding: 4px 8px;
        margin: 8px auto;
        background-color: #FFF;
        border-radius: 16px;
        border: 1px solid #888;
        width: 100%;
    }

    form input[type="submit"] {
        margin: 0 auto;
    }

</style>

<form action="/" method="post">
    <input type="text" placeholder="Ф.И.О." name="FIO" required autocomplete="off"/>
    <input type="text" placeholder="Номер телефона" name="phone" required autocomplete="off"/>
    <p>Дата въезда:</p>
    <input id="date_from" type="date" name="date_from" required autocomplete="off"/>
    <p>Дата отъезда:</p>
    <input id="date_until" type="date" name="date_until" required autocomplete="off"/>
    <input type="submit" value="Отправить" />
</form>
