<style>
    #toTop {
        width: 64px;
        height: 64px;
        border: 0;
        border-radius: 50%;
        background: 0;
        position: fixed;
        bottom: 2rem;
        right: 2rem;
        z-index: 4;
        display: none;
        justify-content: center;
        font-size: 2rem;
        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAADESURBVGhD7dfRCoNAEEPR1f//57WKgUXE6kyGsJALpU/d5tj64NJ7bzO3nu/TZoA6A9QZoM4AdQaoM0CdAeoM+NP+tFT6xFQJGIeXIaoAd4NLEBWAp6F0BBtwHbicrzEqggm4G4/KECzA03hUgmAA3oxHdEQW8GU8oiIygMh4RENEAZnxiIJg3AOR8Sjz2aMIYLxS6QG/xjM+/woRAL6QMR6Fz4z+hZjjUehMxj0gzQB1BqgzQJ0B6gxQZ4A6A9RNDmhtA5nnG12wR/KZAAAAAElFTkSuQmCC');
        background-position: center;
        background-color: #00ceff;
    }

</style>

<button id="toTop" onclick="toTopScroll();"></button>

<script>
    function toTopScroll(){
        scrollToElement('body', 250);
    }
    window.onscroll = function scrollFunction() {
        if (document.body.scrollTop >= window.innerHeight * 0.5 || document.documentElement.scrollTop >= window.innerHeight  * 0.5 ) {
            $("#toTop").fadeIn(250);
        } else {
            $("#toTop").fadeOut(250);
        }
        //$(".slide").height( window.innerHeight );
    }

</script>
