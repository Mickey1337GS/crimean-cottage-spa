<style>
    @keyframes rotate {
        from {
            transform: rotate(0deg);
        }

        to {
            transform: rotate(360deg);
        }
    }

    .circle {
        border: 16px solid #DDD;
        border-top: 16px solid #369;
        border-radius: 50%;
        width: 128px;
        height: 128px;
        max-width: 50%;
        max-height: 50%;
        animation: rotate 1.5s linear infinite;
    }

    #loading_screen {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: #FFF;
        display: flex;
        justify-content: center;
        align-items: center;
        -webkit-backface-visibility: hidden;
        z-index: 5;
    }

    #loading_screen img {
        width: 25%;
    }

</style>

<div id="loading_screen">
    <div class="circle"></div>
</div>

<script>
    $(function() {
        $("#loading_screen").fadeOut(500, function() {
            this.remove();
        });
    })

</script>
