<?php
set_include_path( $_SERVER["DOCUMENT_ROOT"] );
require("app/core.php");
$App->handlePost();
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <title><?= $App->getTitle(); ?></title>
        <?php $App->getHead(); ?>
    </head>
    <body>
        <?php $App->getChunk("loading"); ?>
        <?php $App->getChunk("background_index"); ?>
        <?php $App->getChunk("toTop"); ?>
        <?php $App->getBody(); ?>
    </body>
</html>