var log = console.log;

const Q = document.querySelector.bind(document);

const scrollToElement = function (element, time) {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(element).offset().top
    }, time);
}

$(function () {

    var today = new Date();
    var dd = today.getDate();
    var dd2 = today.getDate() + 1;
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (dd2 < 10) {
        dd2 = '0' + dd2
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    tomorrow = yyyy + '-' + mm + '-' + dd2;
    document.getElementById("date_from").setAttribute("min", today);
    document.getElementById("date_until").setAttribute("min", tomorrow);

    $("#nav a").on("click", function (e) {
        e.preventDefault();
        scrollToElement(e.target.hash, 500);
    });

    $("#carousel").slick({
        autoplay: true,
        arrows: false,
        dots: true,
        //fade: true,
        adaptiveHeight: true
    });

    /*
    window.addEventListener("resize", function(){
        $(".slide").height(window.innerHeight);
    });
    */

    log("Page has been loaded.");
    $("#nav").slideDown();
    $("#logotext").fadeIn(2000);
})
