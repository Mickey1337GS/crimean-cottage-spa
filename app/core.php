<?php

require 'config.php';

class App {
    private $title;
    private $ts;
    private $con;
    public $p;
    
    function __construct( $args ) {     
        $this->title = $args["title"];
        $this->ts = time();
        $this->p = $_POST;
        session_start();
    }
    
    function getBoardTemp(){
        (int)$t = exec("cat /sys/class/thermal/thermal_zone0/temp");
        return number_format($t/1000, 1);
    }
    
    function handlePost(){
        if( !empty( $this->p ) ) {
            if(
                !empty ( $this->p['FIO'] ) &&
                !empty ( $this->p['phone'] ) &&
                !empty ( $this->p['date_from'] ) &&
                !empty ( $this->p['date_until'] )
              ) {
                file_put_contents(
                    "app/sessions/".session_id().".txt",
                    "=============REQUEST============\n".
                    "Name: ".$this->p['FIO']."\n".
                    "Phone: ".$this->p['phone']."\n".
                    "From: ".$this->p['date_from']."\n".
                    "Until: ".$this->p['date_until']."\n".
                    "\n",
                    FILE_APPEND
                );
                header('Location: /');
                exit;
            } 
        } else {

        }
    }
    
    function getPost() { var_dump( $this->p ); }
    function getTimestamp() { return $this->ts; }  
    function getTitle(){ return $this->title; }  
    function getHead(){ global $App; include_once("template/head.php"); }
    function getBody(){ global $App; include_once("template/body.php"); }
    function getChunk($c){ global $App; include("chunks/$c.php"); }
    function getPhotos($f){
        $pics = array_diff( scandir( $f ), array('..', '.')) ;
        foreach($pics as $p) {
            echo "<img src='".$f.'/'.$p."' alt='img'/>";
        }
    }
    
    /* DB */    
    function connectToDB(){
        $this->con = new mysqli( "127.0.0.1", DB_USER, DB_PASS );
        if (!$this->con) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
    }
    
    function useDB( $DB ){
        $q = "USE $DB";
        $res = $this->con->query($q);
        if( !$res ) {
            echo "Couldn't use database '$DB'. Does it exist?<br>";
            return false;
        } else {
            echo "Connected to DB: '$DB'<br>";
            return true;
        }
    }
    
    function createDB( $DB ){
        $q = "CREATE DATABASE $DB";
        $res = $this->con->query($q);
        if ( $res ) {
            echo "Database $DB has been created<br>";
            return true;
        } else {
            echo "Couldn't create database $DB. Do you have the rights?<br>";
            return false;
        }
    }
    
    function ifTableExists( $name ){
        $q = "SHOW TABLES LIKE '$name'";
        $res = $this->con->query($q);
        if ( $res->num_rows === 0 ) {
            echo "Table '$name' does NOT exist.<br>";
            return false;
        } else {            
            echo "Table '$name' exists.<br>";
            return true;
        }
    }
    
    function makeTable( $name ){
        $q = 
            "CREATE TABLE IF NOT EXISTS $name (
            id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
            name VARCHAR(32) NOT NULL,
            email VARCHAR(64) NOT NULL,
            password VARCHAR(32) NOT NULL,
            status VARCHAR(32) NOT NULL DEFAULT 'active',
            registered TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            )  ENGINE=".DB_ENGINE;
        $res = $this->con->query($q);
        if ( $res ) {
            echo "Table '$name' created <br>";
            return true;
        } else {
            echo "Couldn't create table '$name'. Do you have the rights?<br>";
            echo $this->con->error;
            return false;
        }
    }
    
    function getTable( $name ){
        $q = "SELECT * FROM $name";
        $res = $this->con->query($q);
        if( $res->num_rows > 0 ){
            
            $fields = $res->fetch_assoc();
            echo "<div style='display: flex;'>";
            foreach( $fields as $key => $value ) {                
                echo $this->span($key);                
            }
            echo "</div>";
            
            foreach( $res as $key => $value ) {
                echo "<div style='display: flex;'>";
                echo $this->span( $value["id"] );
                echo $this->span( $value["name"] );
                echo $this->span( $value["email"] );
                echo $this->span( $value["password"] );
                echo $this->span( $value["status"] );
                echo $this->span( $value["registered"] );
                echo "</div>";
            }
        } else {
            
        }
    }
    
    
}



$App = new App( [ "title" => APP_TITLE ] );
