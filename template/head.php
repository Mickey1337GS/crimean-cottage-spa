<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans|Russo+One|Open+Sans+Condensed:300&display=swap" rel="stylesheet">

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />

<link href="/libs/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/libs/slick.css" rel="stylesheet" type="text/css" />
<link href="/libs/slick-theme.css" rel="stylesheet" type="text/css" />

<link href="/css/style.css" rel="stylesheet" type="text/css" />

<script src="/libs/jquery-3.3.1.min.js"></script>
<script src="/libs/slick.min.js"></script>

<script src="/js/script.js"></script>


<link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="manifest" href="/manifest.json">