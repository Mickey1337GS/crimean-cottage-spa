<div class="container-fluid">

    <div id="index" class="row slide">
        <div class="container">
            <div class="row">
                <div id="logotext" class="pulsating">
                    <h3>Аренда <span>коттеджа</span> в <b class="grf">Крыму</b></h3>
                </div>
                <div id="index_text" class="col-12 just_flex">
                </div>
                <div id="nav">
                    <a href="#photos">Фото</a>
                    <a href="#contacts">Адрес</a>
                    <a href="#inventory">Инвентарь</a>
                    <a href="#rent">Бронь</a>
                </div>
            </div>
        </div>
    </div>


    <div id="photos" class="row slide just_flex">
        <div class="container">
            <div class="row">
                <h2 class="col-12 header_photos">Фото</h2>
                <div id="carousel_wrapper" class="col-12">
                    <div id="carousel">
                        <?php $App->getPhotos("i"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contacts" class="row slide">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="contact section">Адрес</p>
                    <iframe src="https://yandex.ru/map-widget/v1/-/CGeE7Dp-&scroll=false" frameborder="0" allowfullscreen="true"></iframe>
                    <p class="phone section">+7 (123) 456-78-90</p>
                    <div class="address">
                        <p>Россия, Республика Крым, Черноморский район, село Новосельское</p>
                        <p>Контактное лицо: Иванов Иван Иванович</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="inventory" class="row slide">

        <div class="container">
            <div class="row">
                <div class="col-6"><b>Cпальных мест:</b> 4</div>
                <div class="col-6"><b>Кроватей:</b> 2</div>
                <div class="col-6">
                    <b>Мультимедиа:</b>
                    <ul>
                        <li>Wi-Fi</li>
                        <li>Телевизор</li>
                    </ul>
                </div>
                <div class="col-6">
                    <b>Бытовая техника:</b>
                    <ul>
                        <li>Плита</li>
                        <li>Микроволновка</li>
                        <li>Холодильник</li>
                        <li>Фен</li>
                        <li>Утюг</li>
                    </ul>
                </div>

                <div class="col-6">
                    <p><b>Комфорт:</b> Кондиционер</p>
                </div>
                <div class="col-6">
                    <p><b>Дополнительно:</b> Можно с детьми</p>
                </div>

                <div class="col-12"></div>
            </div>
        </div>
    </div>

    <div id="rent" class="row slide just_flex">
        <?php $App->getChunk("background_rent"); ?>

        <div class="container">
            <div class="row">
                <div class="col-md-3 col-1"></div>
                <div class="col-md-6 col-10">
                    <?php $App->getChunk("form_rent"); ?>
                </div>
                <div class="col-md-3 col-1"></div>
            </div>
        </div>
    </div>

    <div id="footer" class="row slide just_flex">
        <div class="container">
            <div class="row">
                <div class="col-12"><span> Температура платы: <?= $App->getBoardTemp(); ?> °C </span></div>
            </div>
        </div>
    </div>

</div>
